const salesReceiptEmail = (mailContent) =>{

    var saleMailContent =  '   <!doctype html>  '  + 
'   <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"  '  + 
'       xmlns:o="urn:schemas-microsoft-com:office:office">  '  + 
'     '  + 
'   <head>  '  + 
'       <title></title>  '  + 
'       <!--[if !mso]><!-- -->  '  + 
'       <meta http-equiv="X-UA-Compatible" content="IE=edge">  '  + 
'       <!--<![endif]-->  '  + 
'       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  '  + 
'       <meta name="viewport" content="width=device-width,initial-scale=1">  '  + 
'       <style type="text/css">  '  + 
'           #outlook a {  '  + 
'               padding: 0;  '  + 
'           }  '  + 
'     '  + 
'           .ReadMsgBody {  '  + 
'               width: 100%;  '  + 
'           }  '  + 
'     '  + 
'           .ExternalClass {  '  + 
'               width: 100%;  '  + 
'           }  '  + 
'     '  + 
'           .ExternalClass * {  '  + 
'               line-height: 100%;  '  + 
'           }  '  + 
'     '  + 
'           body {  '  + 
'               margin: 0;  '  + 
'               padding: 0;  '  + 
'               -webkit-text-size-adjust: 100%;  '  + 
'               -ms-text-size-adjust: 100%;  '  + 
'           }  '  + 
'     '  + 
'           table,  '  + 
'           td {  '  + 
'               border-collapse: collapse;  '  + 
'               mso-table-lspace: 0pt;  '  + 
'               mso-table-rspace: 0pt;  '  + 
'           }  '  + 
'     '  + 
'           img {  '  + 
'               border: 0;  '  + 
'               height: auto;  '  + 
'               line-height: 100%;  '  + 
'               outline: none;  '  + 
'               text-decoration: none;  '  + 
'               -ms-interpolation-mode: bicubic;  '  + 
'           }  '  + 
'     '  + 
'           p {  '  + 
'               display: block;  '  + 
'               margin: 13px 0;  '  + 
'           }  '  + 
'       </style>  '  + 
'       <!--[if !mso]><!-->  '  + 
'       <style type="text/css">  '  + 
'           @media only screen and (max-width:480px) {  '  + 
'               @-ms-viewport {  '  + 
'                   width: 320px;  '  + 
'               }  '  + 
'     '  + 
'               @viewport {  '  + 
'                   width: 320px;  '  + 
'               }  '  + 
'           }  '  + 
'       </style>  '  + 
'       <!--<![endif]-->  '  + 
'       <!--[if mso]>  '  + 
'     <xml>  '  + 
'     <o:OfficeDocumentSettings>  '  + 
'       <o:AllowPNG/>  '  + 
'       <o:PixelsPerInch>96</o:PixelsPerInch>  '  + 
'     </o:OfficeDocumentSettings>  '  + 
'     </xml>  '  + 
'     <![endif]-->  '  + 
'       <!--[if lte mso 11]>  '  + 
'     <style type="text/css">  '  + 
'       .outlook-group-fix { width:100% !important; }  '  + 
'     </style>  '  + 
'     <![endif]-->  '  + 
'       <style type="text/css">  '  + 
'           @media only screen and (min-width:480px) {  '  + 
'               .mj-column-per-70 {  '  + 
'                   width: 70% !important;  '  + 
'                   max-width: 70%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-30 {  '  + 
'                   width: 30% !important;  '  + 
'                   max-width: 30%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-100 {  '  + 
'                   width: 100% !important;  '  + 
'                   max-width: 100%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-50 {  '  + 
'                   width: 50% !important;  '  + 
'                   max-width: 50%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-80 {  '  + 
'                   width: 80% !important;  '  + 
'                   max-width: 80%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-20 {  '  + 
'                   width: 20% !important;  '  + 
'                   max-width: 20%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-40 {  '  + 
'                   width: 40% !important;  '  + 
'                   max-width: 40%;  '  + 
'               }  '  + 
'     '  + 
'               .mj-column-per-60 {  '  + 
'                   width: 60% !important;  '  + 
'                   max-width: 60%;  '  + 
'               }  '  + 
'           }  '  + 
'       </style>  '  + 
'       <style type="text/css">  '  + 
'           @media only screen and (max-width:480px) {  '  + 
'               table.full-width-mobile {  '  + 
'                   width: 100% !important;  '  + 
'               }  '  + 
'     '  + 
'               td.full-width-mobile {  '  + 
'                   width: auto !important;  '  + 
'               }  '  + 
'           }  '  + 
'       </style>  '  + 
'   </head>  '  + 
'     '  + 
'   <body style="background-color:##F5F5F5;">  '  + 
'       <div>  '  + 
'           <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'           <div style="background:#000000;background-color:#000000;Margin:0px auto;max-width:600px;">  '  + 
'               <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                   style="background:#000000;background-color:#000000;width:100%;">  '  + 
'                   <tbody>  '  + 
'                       <tr>  '  + 
'                           <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">  '  + 
'                               <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-top:0px;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-70 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <!-- <mj-image src="./images/bbapp-sales-receipt-icon.png" alt="BB-App"   /> -->  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="center"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                                       role="presentation"  '  + 
'                                                                       style="border-collapse:collapse;border-spacing:0px;">  '  + 
'                                                                       <tbody>  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td style="width:370px;"><img alt="BB-App"  '  + 
'                                                                                       height="auto"  '  + 
'                                                                                       src="https://i.imgur.com/LewDQqq.png"  '  + 
'                                                                                       style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;"  '  + 
'                                                                                       width="370"></td>  '  + 
'                                                                           </tr>  '  + 
'                                                                       </tbody>  '  + 
'                                                                   </table>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:180px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-30 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="left"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;padding-left:0;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:20px;line-height:1;text-align:left;color:#ffffff;">  '  + 
'                                                                       Official Store<br>Receipt</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 
'                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;padding-bottom:0px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;letter-spacing:0px;line-height:0px;text-align:right;color:#ffffff;">  '  + 
'                                                                       '+mailContent.orderDate+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Order# '+mailContent.orderNo+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 
'                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" width="100%">  '  + 
'                                                           <tbody>  '  + 
'                                                               <tr>  '  + 
'                                                                   <td style="vertical-align:top;padding-bottom:-10px;">  '  + 
'                                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                                           role="presentation" width="100%">  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td align="left"  '  + 
'                                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                                   <div  '  + 
'                                                                                       style="font-family:helvetica;font-size:16px;line-height:0px;text-align:left;color:#ffffff;">  '  + 
'                                                                                       '+mailContent.contactDetails.firstName+' '+mailContent.contactDetails.lastname+'</div>  '  + 
'                                                                               </td>  '  + 
'                                                                           </tr>  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td align="left"  '  + 
'                                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                                   <div  '  + 
'                                                                                       style="font-family:helvetica;font-size:16px;line-height:0px;text-align:left;color:#ffffff;">  '  + 
'                                                                                       '+mailContent.contactDetails.add1+'</div>  '  + 
'                                                                               </td>  '  + 
'                                                                           </tr>  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td align="left"  '  + 
'                                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                                   <div  '  + 
'                                                                                       style="font-family:helvetica;font-size:16px;line-height:0px;text-align:left;color:#ffffff;">  '  + 
'                                                                                       '+mailContent.contactDetails.add2+'</div>  '  + 
'                                                                               </td>  '  + 
'                                                                           </tr>  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td align="left"  '  + 
'                                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                                   <div  '  + 
'                                                                                       style="font-family:helvetica;font-size:16px;line-height:0px;text-align:left;color:#ffffff;">  '  + 
'                                                                                       '+mailContent.contactDetails.city+', '+mailContent.contactDetails.state+', '+mailContent.contactDetails.zip+'</div>  '  + 
'                                                                               </td>  '  + 
'                                                                           </tr>  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td align="left"  '  + 
'                                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                                   <div  '  + 
'                                                                                       style="font-family:helvetica;font-size:16px;line-height:0px;text-align:left;color:#ffffff;">  '  + 
'                                                                                       '+mailContent.contactDetails.country+'</div>  '  + 
'                                                                               </td>  '  + 
'                                                                           </tr>  '  + 
'                                                                           <tr>  '  + 
'                                                                               <td align="left"  '  + 
'                                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                                   <div  '  + 
'                                                                                       style="font-family:helvetica;font-size:16px;line-height:0px;text-align:left;color:#ffffff;">  '  + 
'                                                                                       '+mailContent.contactDetails.fname+'</div>  '  + 
'                                                                               </td>  '  + 
'                                                                           </tr>  '  + 
'                                                                       </table>  '  + 
'                                                                   </td>  '  + 
'                                                               </tr>  '  + 
'                                                           </tbody>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 



salesItemRenderEmail(mailContent.item)+
'                               <!--[if mso | IE]></td></tr></table></td></tr><![endif]-->  '  + 
'                               <!-- <mj-section>  '  + 
'       <mj-column width="100%">  '  + 
'             '  + 
'       </mj-column>  '  + 
'       <mj-column width="100%">  '  + 
'         '  + 
'             '  + 
'       </mj-column>  '  + 
'   </mj-section> -->  '  + 


'                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:30px;padding-top:30px;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:480px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-80 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Subtotal:</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Boints:</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Shipping:</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Tax:</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Total:</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:120px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-20 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       '+mailContent.paymentDetails.boints+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       '+mailContent.paymentDetails.boints+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       '+mailContent.paymentDetails.shippingCharge+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       '+mailContent.paymentDetails.tax+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       '+mailContent.paymentDetails.total+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 
'                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:240px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-40 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="center"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:center;color:#ffffff;">  '  + 
'                                                                       PAID '+mailContent.paymentDetails.paidDate+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:360px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-60 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="left"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color:#ffffff;">  '  + 
'                                                                       '+mailContent.paymentDetails.cardType.toUpperCase()+' '+mailContent.paymentDetails.cardNo+'</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 
'                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:480px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-80 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="left"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color:#ffffff;">  '  + 
'                                                                       Your package will be shipped within '+mailContent.shipDetails.shipWithIn+' and you  '  + 
'                                                                       should recieve it within '+mailContent.shipDetails.recieveWithIn+'.</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 
'                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
'                               <div style="Margin:0px auto;max-width:600px;">  '  + 
'                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
'                                       style="width:100%;">  '  + 
'                                       <tbody>  '  + 
'                                           <tr>  '  + 
'                                               <td  '  + 
'                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;text-align:center;vertical-align:top;">  '  + 
'                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->  '  + 
'                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
'                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
'                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
'                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
'                                                                       Thanks for your order!</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                           <tr>  '  + 
'                                                               <td align="right"  '  + 
'                                                                   style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">  '  + 
'                                                                   <div  '  + 
'                                                                       style="font-family:helvetica;font-size:22px;font-weight:500;line-height:1;text-align:right;color:#e90086;">  '  + 
'                                                                       Badger, Baby!</div>  '  + 
'                                                               </td>  '  + 
'                                                           </tr>  '  + 
'                                                       </table>  '  + 
'                                                   </div>  '  + 
'                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'                                               </td>  '  + 
'                                           </tr>  '  + 
'                                       </tbody>  '  + 
'                                   </table>  '  + 
'                               </div>  '  + 
'                               <!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->  '  + 
'                           </td>  '  + 
'                       </tr>  '  + 
'                   </tbody>  '  + 
'               </table>  '  + 
'           </div>  '  + 
'           <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
'       </div>  '  + 
'   </body>  '  + 
'     '  + 
'  </html>  ' ; 


return saleMailContent
}

exports.salesReceiptEmail = salesReceiptEmail;


const salesItemRenderEmail =(items) => {
    var itemEmailContent = "";
    var topDiverder = '                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
    '                               <div style="Margin:0px auto;max-width:600px;">  '  + 
    '                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
    '                                       style="width:100%;">  '  + 
    '                                       <tbody>  '  + 
    '                                           <tr>  '  + 
    '                                               <td  '  + 
    '                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;vertical-align:top;">  '  + 
    '                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->  '  + 
    '                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
    '                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
    '                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
    '                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
    '                                                           <tr>  '  + 
    '                                                               <td  '  + 
    '                                                                   style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">  '  + 
    '                                                                   <p  '  + 
    '                                                                       style="border-top:solid 3px lightgrey;font-size:1;margin:0px auto;width:100%;">  '  + 
    '                                                                   </p>  '  + 
    '                                                                   <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 3px lightgrey;font-size:1;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;  '  + 
    '   </td></tr></table><![endif]-->  '  + 
    '                                                               </td>  '  + 
    '                                                           </tr>  '  + 
    '                                                       </table>  '  + 
    '                                                   </div>  '  + 
    '                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
    '                                               </td>  '  + 
    '                                           </tr>  '  + 
    '                                       </tbody>  '  + 
    '                                   </table>  '  + 
    '                               </div>  '  ;
    itemEmailContent += topDiverder;


    items.forEach((item)=>{
        itemEmailContent += '                               <!--[if mso | IE]><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
        '                               <div style="Margin:0px auto;max-width:600px;">  '  + 
        '                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
        '                                       style="width:100%;">  '  + 
        '                                       <tbody>  '  + 
        '                                           <tr>  '  + 
        '                                               <td  '  + 
        '                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;text-align:center;vertical-align:top;">  '  + 
        '                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->  '  + 
        '                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
        '                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
        '                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
        '                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
        '                                                           <tr>  '  + 
        '                                                               <td align="left"  '  + 
        '                                                                   style="font-size:0px;padding:10px 25px;padding-top:0px;word-break:break-word;">  '  + 
        '                                                                   <div  '  + 
        '                                                                       style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color:#ffffff;">  '  + 
        '                                                                       '+item.itemName+'</div>  '  + 
        '                                                               </td>  '  + 
        '                                                           </tr>  '  + 
        '                                                       </table>  '  + 
        '                                                   </div>  '  + 
        '                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
        '                                               </td>  '  + 
        '                                           </tr>  '  + 
        '                                       </tbody>  '  + 
        '                                   </table>  '  + 
        '                               </div>  '  + 
        '                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
        '                               <div style="Margin:0px auto;max-width:600px;">  '  + 
        '                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
        '                                       style="width:100%;">  '  + 
        '                                       <tbody>  '  + 
        '                                           <tr>  '  + 
        '                                               <td  '  + 
        '                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-top:0px;text-align:center;vertical-align:top;">  '  + 
        '                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:600px;" ><![endif]-->  '  + 
        '                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
        '                                                       style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">  '  + 
        '                                                       <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td style="vertical-align:top;width:300px;" ><![endif]-->  '  + 
        '                                                       <div class="mj-column-per-50 outlook-group-fix"  '  + 
        '                                                           style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">  '  + 
        '                                                           <table border="0" cellpadding="0" cellspacing="0"  '  + 
        '                                                               role="presentation" style="vertical-align:top;"  '  + 
        '                                                               width="100%">  '  + 
        '                                                               <tr>  '  + 
        '                                                                   <td align="left"  '  + 
        '                                                                       style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;padding-left:55px;word-break:break-word;">  '  + 
        '                                                                       <div  '  + 
        '                                                                           style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color:#ffffff;">  '  + 
        '                                                                           '+getItemSize(item.size)+'</div>  '  + 
        '                                                                   </td>  '  + 
        '                                                               </tr>  '  + 
        '                                                           </table>  '  + 
        '                                                       </div>  '  + 
        '                                                       <!--[if mso | IE]></td><td style="vertical-align:top;width:300px;" ><![endif]-->  '  + 
        '                                                       <div class="mj-column-per-50 outlook-group-fix"  '  + 
        '                                                           style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">  '  + 
        '                                                           <table border="0" cellpadding="0" cellspacing="0"  '  + 
        '                                                               role="presentation" style="vertical-align:top;"  '  + 
        '                                                               width="100%">  '  + 
        '                                                               <tr>  '  + 
        '                                                                   <td align="right"  '  + 
        '                                                                       style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">  '  + 
        '                                                                       <div  '  + 
        '                                                                           style="font-family:helvetica;font-size:16px;line-height:1;text-align:right;color:#ffffff;">  '  + 
        '                                                                           '+item.price+'</div>  '  + 
        '                                                                   </td>  '  + 
        '                                                               </tr>  '  + 
        '                                                           </table>  '  + 
        '                                                       </div>  '  + 
        '                                                       <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
        '                                                   </div>  '  + 
        '                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
        '                                               </td>  '  + 
        '                                           </tr>  '  + 
        '                                       </tbody>  '  + 
        '                                   </table>  '  + 
        '                               </div>  '  + 
        '                               <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->  '  + 
        '                               <div style="Margin:0px auto;max-width:600px;">  '  + 
        '                                   <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"  '  + 
        '                                       style="width:100%;">  '  + 
        '                                       <tbody>  '  + 
        '                                           <tr>  '  + 
        '                                               <td  '  + 
        '                                                   style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0px;text-align:center;vertical-align:top;">  '  + 
        '                                                   <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->  '  + 
        '                                                   <div class="mj-column-per-100 outlook-group-fix"  '  + 
        '                                                       style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">  '  + 
        '                                                       <table border="0" cellpadding="0" cellspacing="0"  '  + 
        '                                                           role="presentation" style="vertical-align:top;" width="100%">  '  + 
        '                                                           <tr>  '  + 
        '                                                               <td  '  + 
        '                                                                   style="font-size:0px;padding:10px 25px;padding-top:0px;padding-bottom:0px;word-break:break-word;">  '  + 
        '                                                                   <p  '  + 
        '                                                                       style="border-top:solid 3px lightgrey;font-size:1;margin:0px auto;width:100%;">  '  + 
        '                                                                   </p>  '  + 
        '                                                                   <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 3px lightgrey;font-size:1;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;  '  + 
        '   </td></tr></table><![endif]-->  '  + 
        '                                                               </td>  '  + 
        '                                                           </tr>  '  + 
        '                                                       </table>  '  + 
        '                                                   </div>  '  + 
        '                                                   <!--[if mso | IE]></td></tr></table><![endif]-->  '  + 
        '                                               </td>  '  + 
        '                                           </tr>  '  + 
        '                                       </tbody>  '  + 
        '                                   </table>  '  + 
        '                               </div>  '  ;

    })





    return itemEmailContent;
}

const getItemSize = (size) => {

    if(size){
        return 'Size '+size;
    }else{
        return '';
    }
}


